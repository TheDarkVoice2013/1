﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Fire : MonoBehaviour {
    public float FryingPower;
    public Grill myGrill;
    public SpriteRenderer CoalsImage;
    public float maxPowerColor = 100;
    public float firstStageColor = 2f;
    public float power;
    public float powerFromIgniter;
    public int totalNumber;
    public List<FuelManager> FuelManagers = new List<FuelManager>();
    public List<Meat> Meats = new List<Meat>();
    float bigTimer;
    float timer;
    public float totalBurning;
    public TextScrip textscrip;
    public float timeModifier = 0.5f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        timer += Time.deltaTime;
        if (power < 0.5 && power > 0)
            CoalsImage.color = Color.Lerp(Color.white, Color.yellow, (float)power / 0.5f);
        if (power > 0.5f)
        {
            CoalsImage.color = Color.Lerp(Color.yellow, Color.red, power / 5f);
        }
        while (bigTimer > 1f)
        {
            UpdateFryingPower();
            bigTimer -= 1f;

        }

        while (timer >= 0.2f)
        {
            totalBurning = 0;
            totalNumber = 0;

            if (powerFromIgniter > 0)
            {
                powerFromIgniter -= myGrill.powerLoss * timeModifier;
                if (powerFromIgniter < 0)
                    powerFromIgniter = 0;
            }
            power = powerFromIgniter;
            foreach (FuelManager fuelManager in FuelManagers)
            {
                power += fuelManager.myBurnPower;
                totalNumber += fuelManager.currentNumber;

            }

            foreach (FuelManager fuelManager in FuelManagers)
            {
                if (totalNumber > 0)
                {
                    fuelManager.AddBurn(power * fuelManager.currentNumber / totalNumber);

                }
                fuelManager.update();
                totalBurning += fuelManager.burnPower;

            }

            foreach (Meat meat in Meats)
            {
                meat.AddBurn(power);
            }

            textscrip.target = totalNumber;


            timer -= 0.2f;
            bigTimer += 0.2f;

        }
    }

    public void UpdateFryingPower()
    {
        FryingPower = power;
    }

    public void Ignite(Igniter igniter)
    {
        if (powerFromIgniter < igniter.basePower / 2)
        {
            powerFromIgniter = igniter.basePower;
        }
        else if (powerFromIgniter < igniter.basePower * 2)
        {
            powerFromIgniter += igniter.basePower / 4;
        }
    }

    public void AddMeat(Meat meat)
    {
        Meats.Add(meat);
    }

    public void AddFuel(Fuel fuel, int howMany)
    {
        foreach (FuelManager fuelManager in FuelManagers)
        {
            if (fuel == fuelManager.fuelType)
            {
                fuelManager.Add(howMany);
                return;
            }
        }
        FuelManagers.Add(new FuelManager(fuel));
        FuelManagers[FuelManagers.Count-1].Add(howMany);
    }

    public void AddFuel (Fuel fuel)
    {
        AddFuel(fuel, 1);
    }


}
