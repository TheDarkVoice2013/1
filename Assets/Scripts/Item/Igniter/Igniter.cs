﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu()]
public class Igniter : ScriptableObject {

    public float basePower;
    public float maxPower;
    public int uses;
}
