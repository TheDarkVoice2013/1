﻿using UnityEngine;
using System.Collections;

public abstract class Fuel : Item {

    public string Name;
    public float burnPower;
    public float burnPerFuel;
    public float flavour;
    public float MaxBurning;
    public float burnNeeded;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
