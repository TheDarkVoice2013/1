﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MeatObject : MonoBehaviour {

    public int id;

    public SellingState currentSellingState;

    public Image myImage;

	// Use this for initialization
	void Start ()
    {
        currentSellingState = FindObjectOfType<GameState>().mySellingState;
        
	}
	
	// Update is called once per frame
	void Update () {

        // To modify to increase performance, to make the update update slower, create a float and etc

        if (currentSellingState.fires[currentSellingState.currentId].Meats[id])
        {
            myImage = gameObject.AddComponent<Image>();
            myImage.sprite = FindObjectOfType<GameState>().MeatSprites[0];
        }
        else
        {
            if (gameObject.GetComponent<Image>())
            {
                Destroy(gameObject.GetComponent<Image>());
            }
        }
    }
}
