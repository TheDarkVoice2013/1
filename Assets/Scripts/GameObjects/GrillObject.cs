﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class GrillObject : MonoBehaviour {

    public Image myImage;
    public SellingState sellingState;
    public int id;

	// Use this for initialization
	void Start ()
    {
        myImage = GetComponent<Image>();
        sellingState = FindObjectOfType<GameState>().mySellingState;

        if (id < sellingState.grills.Count - 1)
        {
            if (sellingState.grills[id] == null)
            {
                myImage.color = Color.white;
            }
        }

        if (id > sellingState.grills.Count - 1)
        {
            myImage.color = Color.white;
        }

    }
	

    public void OpenGrill()

    {
        sellingState.currentGrill = sellingState.grills[id];
        sellingState.currentId = id;
    }


	// Update is called once per frame
	void Update ()
    {
	
	}
}
