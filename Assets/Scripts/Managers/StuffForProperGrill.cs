﻿using UnityEngine;
using System.Collections;

public class StuffForProperGrill : MonoBehaviour {

    public SellingState mySellingState;

    public void AddMeat(Meat meat)
    {
        mySellingState.fires[mySellingState.currentId].AddMeat(meat);
    }

	// Use this for initialization
	void Start ()
    {
        mySellingState = FindObjectOfType<GameState>().mySellingState;
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
