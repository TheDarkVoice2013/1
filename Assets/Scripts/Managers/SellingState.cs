﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SellingState : ScriptableObject {

    public Menu menu;

    public List<Grill> grills;

    public List<Fire> fires = new List<Fire>();

    public Grill currentGrill;
    public int currentId;
    public bool initialized = false;
    // Use this for initialization
    public void start ()
    {

        int i = 0;

        foreach (Grill grill in grills)
        {
            fires.Add(new Fire());
            fires[i].myGrill = grill;
            i++;
        }

        initialized = true;
	}
	
    public SellingState()
    {

    }

    public SellingState(List<Grill> grill)
    {
        grills = grill;
    }

    public SellingState (Grill grill)
    {
        List<Grill> grillss = new List<Grill>();
        grillss.Add(grill);
        grills = grillss;
    }

	// Update is called once per frame
	void Update () {
	
	}
}
