﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {

    SceneManager sm;

	// Use this for initialization
	void Start () {
        sm = FindObjectOfType<SceneManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void LoadLevel(int number)
    {
        sm.LoadScene(number);
    }

}
