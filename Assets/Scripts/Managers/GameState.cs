﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameState : MonoBehaviour {

    public SellingState mySellingState;

    public Grill gril;

    public Sprite[] MeatSprites;
	// Use this for initialization
	void Awake () {

        mySellingState = new SellingState(gril);

	}
	
	// Update is called once per frame
	void Update ()
    {
	    // Increase performance here

        if(!mySellingState.initialized)
        {
            mySellingState.start();
        }


	}
}
