﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class SceneManager : MonoBehaviour {

    public List<GameObject> objects;

    public void LoadScene (int path)
    {
        Application.LoadLevel(path);
    }



	// Use this for initialization
	void Start ()
    {
	    foreach (GameObject objec in objects)
        {
            SceneManager.DontDestroyOnLoad(objec);
        }

        StartCoroutine(wait());

    }
	
	// Update is called once per frame
	void Update () {
	
	}
    IEnumerator wait()
    {
        yield return new WaitForSeconds(0.2f);
        Application.LoadLevel(1);

    }


}
